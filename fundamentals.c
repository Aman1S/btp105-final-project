#include "fundamentals.h" 
void fundamentals () {
  printf("*** Start of Indexing Strings Demo ***\n");
  char buffer1[80];         //declared a character named buffer1 to store a number or string later
  char num_input[10];       //declare a character named num_input 
  int position;
  while (TRUE) {
    printf ("Type a string (q - to quit):\n"); //asks to type a string or you can end loop by pressing q
    gets(buffer1);            //the string you enter will become buffer1 
    if (strcmp(buffer1, "q") == 0) break;   //if buffer1 is equal to 0 than it breaks the loop
    while (TRUE) {             
      printf ("Type the character position within the string:\n");  //if buffer1 holds true then this statement prints
      gets(num_input);              //the number you input will be character num_input
      position = atoi(num_input);     //matches the number to the letter position
      if (position >=strlen(buffer1)) {         //means if the position number is greater than the string length of buffer1
        printf("Wrong position... type again, please\n");  //then this statement prints and loops back to ask character position
        continue;
       }
      printf("The character found at %d position is \ '%c\ ' \n", position, buffer1[position]); //statement shows the letter of the string mathcing the input character position
      break;   //loop breaks after pressing q
     }
  }
 printf("*** End of Indexing Strings Demo ***\n\n");    //message displays after loop is ended
//version 2
 printf("*** Start of Measuring Strings Demo ***\n");  //start of loop 2
 char buffer2[80];          //declaring character for storage
 while (TRUE) {
   printf("Type a string (q - to quit) : \n");    
   gets(buffer2);                       //the string you type is stored in buffer2
   if (strcmp(buffer2, "q") == 0) break;   //breaks the loop if rules not followed
   printf ("The length is %lu\n", strlen(buffer2));  //if conditions are met then the number of characters in the string is displayed
 }
 printf("*** End of Measuring Strings Demo ***\n\n");  //displayed after loop ends
 //version 3
 printf("*** Start of Copying Strings Demo ***\n");   //start of loop 3
 char destination [80];   //declarition of characters for storage
 char source [80];
 while (TRUE) {
   destination[0] = '\0';           //destination character is set to 0 
   printf("Destination string is reset to empty \n");  
   printf("Type a source string (q - to quit):\n");    
   gets(source);              //the string you typed is stored in char source
   if (strcmp(source, "q") == 0) break;    //if conditions hold true then string is moved and stored in char destination
   strcpy(destination, source);
   printf("New destination string is \'%s\' :\n", destination); 

 }
 printf("*** End of Copying Strings Demo ***\n\n");  //end of the loop message after pressing q to quit
}

